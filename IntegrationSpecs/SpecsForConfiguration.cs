﻿using IntegrationSpecs.TestHelpers;
using NUnit.Framework;
using SpecsFor.Configuration;

namespace IntegrationSpecs
{
    [SetUpFixture]
    public class SpecsForConfig : SpecsForConfiguration
    {
        public SpecsForConfig()
        {
            WhenTesting<INeedDatabase>().EnrichWith<EfCreateDatabaseBehavior>();
            WhenTesting<INeedDatabase>().EnrichWith<TransactionScopeBehavior>();
            WhenTesting<INeedDatabase>().EnrichWith<EfContextFactoryBehavior>();
            WhenTesting<INeedMediator>().EnrichWith<MediatorBehavior>();

        }
    }
}