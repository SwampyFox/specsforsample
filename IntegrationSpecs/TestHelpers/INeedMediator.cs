﻿using SpecsFor;
using SpecsFor.Configuration;
using System.Diagnostics;
using SpecsForSample.Infrastructure;

namespace IntegrationSpecs.TestHelpers
{
    public interface INeedMediator : ISpecs
    {
         
    }

    public class MediatorBehavior : Behavior<INeedMediator>
    {
        public override void SpecInit(INeedMediator instance)
        {
            instance.MockContainer.Configure(cfg => cfg.AddRegistry(new MediatorRegistry()));

            Debug.WriteLine("=================================");
            Debug.WriteLine("=========== WHAT Did I SCAN =======");
            Debug.WriteLine("=================================");
            Debug.Write(instance.MockContainer.WhatDidIScan());
            Debug.WriteLine("=================================");
            Debug.WriteLine("=========== WHAT DO I HAVE =======");
            Debug.WriteLine("=================================");
            Debug.Write(instance.MockContainer.WhatDoIHave());
        }
    }
}