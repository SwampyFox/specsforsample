﻿using SpecsFor;
using SpecsForSample.Data;

namespace IntegrationSpecs.TestHelpers
{
    public interface INeedDatabase : ISpecs
    {
        SampleContext Database { get; set; }
    }
}