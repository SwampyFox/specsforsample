﻿using SpecsFor.Configuration;
using SpecsForSample.Data;

namespace IntegrationSpecs.TestHelpers
{
    public class EfContextFactoryBehavior : Behavior<INeedDatabase>
    {
        public override void SpecInit(INeedDatabase instance)
        {
            instance.Database = new SampleContext();

            instance.MockContainer.Configure(cfg => cfg.For<SampleContext>().Use(instance.Database));
        }

        public override void AfterSpec(INeedDatabase instance)
        {
            instance.Database.Dispose();
        }
    }
}