﻿using System;
using System.Data.Entity;
using System.IO;
using SpecsFor.Configuration;
using SpecsForSample.Data;
using SpecsForSample.Migrations;
namespace IntegrationSpecs.TestHelpers
{
    public class EfCreateDatabaseBehavior : Behavior<INeedDatabase>
    {
        private static bool _isIntialized;

        public override void SpecInit(INeedDatabase instance)
        {
            try
            {
                if (_isIntialized) return;

                //Testing - NCRunch sets this.....
                AppDomain.CurrentDomain.SetData("DataDirectory", Directory.GetCurrentDirectory());

                var strategy = new MigrateDatabaseToLatestVersion<SampleContext, Configuration>();

                Database.SetInitializer(strategy);

                using (var context = new SampleContext())
                {
                    context.Database.Initialize(force: true);
                }

                _isIntialized = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}