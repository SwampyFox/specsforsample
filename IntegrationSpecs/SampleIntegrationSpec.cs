﻿using System.Linq;
using IntegrationSpecs.TestHelpers;
using NUnit.Framework;
using Should;
using SpecsFor;
using SpecsForSample.Controllers;
using SpecsForSample.Data;
using SpecsForSample.Infrastructure;

namespace IntegrationSpecs
{
    public class SampleIntegrationSpec : SpecsFor<HomeController>, INeedDatabase, INeedMediator
    {

        private ProblemType _problemType;

        protected override void Given()
        {


            Database.ProblemTypes.Add(new ProblemType {Description = "A Test Problem Type"});
            Database.SaveChanges();

            _problemType = Database.ProblemTypes.OrderByDescending(pt => pt.Id).First();
        }

        protected override async void When()
        {
            await SUT.Edit(new Home.Command() {Id = _problemType.Id, Description = "Updated Description"});
        }

        [Test]
        public void Then()
        {
            var currentData = Database.ProblemTypes.Single(s => s.Id == _problemType.Id);

            currentData.Description.ShouldEqual("Updated Description");

        }

        public SampleContext Database { get; set; }


    }
}