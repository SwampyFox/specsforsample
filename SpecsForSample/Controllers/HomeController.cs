﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MediatR;
using SpecsForSample.Data;
using SpecsForSample.Infrastructure;

namespace SpecsForSample.Controllers
{
    public class HomeController : Controller
    {
        private readonly SampleContext _db;
        private readonly IMediator _mediator;

        public HomeController(SampleContext db, IMediator mediator)
        {
            _db = db;
            _mediator = mediator;
        }

        public ActionResult Index()
        {

            var data = _db.ProblemTypes.ToList();

            return View(data);
        }

        public async Task<ActionResult> Edit(Home.Query query)
        {
            var command = await _mediator.SendAsync(query);

            return View(command);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(Home.Command command)
        {

            await _mediator.SendAsync(command);

            return RedirectToAction("Index");
        }

    }
}