﻿
using MediatR;

namespace SpecsForSample.Infrastructure
{
    public class MediatorRegistry : StructureMap.Registry
    {
        public MediatorRegistry()
        {
            Scan(s =>
            {

                s.AssemblyContainingType<IMediator>();
                s.AssemblyContainingType<MediatorRegistry>();
                s.ConnectImplementationsToTypesClosing((typeof(IRequestHandler<,>)));
                s.ConnectImplementationsToTypesClosing((typeof(INotificationHandler<>)));
                s.ConnectImplementationsToTypesClosing(typeof(IAsyncRequestHandler<,>));
                s.ConnectImplementationsToTypesClosing(typeof(IAsyncNotificationHandler<>));
                s.WithDefaultConventions();
            });

            For<SingleInstanceFactory>().Use<SingleInstanceFactory>(ctx => t => ctx.GetInstance(t));
            For<MultiInstanceFactory>().Use<MultiInstanceFactory>(ctx => t => ctx.GetAllInstances(t));
            For<IMediator>().Use<Mediator>();
        }
    }
}