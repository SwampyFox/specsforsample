﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using MediatR;
using SpecsForSample.Data;

namespace SpecsForSample.Infrastructure
{
    public class Home
    {
        public class Query : IAsyncRequest<Command>
        {
            public int? Id { get; set; }
        }

        public class QueryHandler : IAsyncRequestHandler<Query, Command>
        {
            private readonly SampleContext _db;

            public QueryHandler(SampleContext db)
            {
                _db = db;
            }

            public async Task<Command> Handle(Query message)
            {
                return
                    await
                        _db.ProblemTypes.Select(s => new Command {Id = s.Id, Description = s.Description})
                            .SingleOrDefaultAsync(m => m.Id == message.Id);
            }
        }

        public class Command : IAsyncRequest
        {
            [HiddenInput]
            public int Id { get; set; }

            [Required, MaxLength(50)]
            public string Description { get; set; }
        }


        public class Handler : AsyncRequestHandler<Command>
        {
            private readonly SampleContext _db;

            public Handler(SampleContext db)
            {
                _db = db;
            }


            protected override async Task HandleCore(Command message)
            {
                var problemType = await _db.ProblemTypes.SingleAsync(s=>s.Id == message.Id);

                problemType.Description = message.Description;

                _db.SaveChanges();
            }
        }
    }
}