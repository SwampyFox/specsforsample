﻿using System.Web.Mvc;
using Heroic.Web.IoC;
using SpecsForSample.Data;

namespace SpecsForSample.Infrastructure
{
    public class MvcTransactionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var context = StructureMapContainerPerRequestModule.Container.GetInstance<SampleContext>();
            context.BeginTransaction();
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var instance = StructureMapContainerPerRequestModule.Container.GetInstance<SampleContext>();
            instance.CloseTransaction(filterContext.Exception);
        }
    }
}