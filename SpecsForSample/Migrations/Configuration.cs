using SpecsForSample.Data;

namespace SpecsForSample.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public sealed class Configuration : DbMigrationsConfiguration<SpecsForSample.Data.SampleContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SpecsForSample.Data.SampleContext context)
        {
/*
            context.ProblemTypes.AddOrUpdate(p=>p.Description, 
                new Data.ProblemType {Description = "Problem Type 1"},
                new Data.ProblemType { Description = "Problem Type 2" },
                new Data.ProblemType { Description = "Problem Type 3" },
                new Data.ProblemType { Description = "Problem Type 4" },
                new Data.ProblemType { Description = "Problem Type 5" },
                new Data.ProblemType { Description = "Problem Type 6" },
                new Data.ProblemType { Description = "Problem Type 7" },
                new Data.ProblemType { Description = "Problem Type 8" },
                new Data.ProblemType { Description = "Problem Type 9" },
                new Data.ProblemType { Description = "Problem Type 10" },
                new Data.ProblemType { Description = "Problem Type 11" });
                */
        }
    }
}