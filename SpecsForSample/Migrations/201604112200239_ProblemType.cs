namespace SpecsForSample.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProblemType : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProblemTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ProblemTypes");
        }
    }
}
