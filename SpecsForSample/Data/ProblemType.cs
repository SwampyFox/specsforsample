﻿using System.ComponentModel.DataAnnotations;

namespace SpecsForSample.Data
{
    public class ProblemType
    {
        public int Id { get; set; }

        [Required, MaxLength(50)]
        public string Description { get; set; }

    }
}