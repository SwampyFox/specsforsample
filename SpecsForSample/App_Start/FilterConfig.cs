﻿using System.Web;
using System.Web.Mvc;
using SpecsForSample.Infrastructure;

namespace SpecsForSample
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());

            filters.Add(new MvcTransactionFilter());
        }
    }
}
